import BookshelfClient from './clients/BookshelfClient';
import ObjectionClient from './clients/ObjectionClient';
import KnexConfig from './config/KnexConfig';
import DatabaseService from './DatabaseService';

class DatabaseFactory {

  public generateServiceUsingObjectionClient(
    client: string,
    host: string,
    port: number,
    database: string,
    user: string,
    password: string,
    searchPath?: string,
  ): DatabaseService {
    const knexConfig = new KnexConfig(client, database, host, password, port, user, searchPath);
    const objection = new ObjectionClient(knexConfig);

    return new DatabaseService(objection);
  }
}

export { DatabaseFactory };
