class KnexConfig {
  protected client: string;
  protected database: string;
  protected host: string;
  protected password: string;
  protected port: number;
  protected user: string;
  protected searchPath?: string;

  constructor(
    client: string,
    database: string,
    host: string,
    password: string,
    port: number,
    user: string,
    searchPath?: string,
  ) {
    this.setClient(client);
    this.setDatabase(database);
    this.setHost(host);
    this.setPassword(password);
    this.setPort(port);
    this.setUser(user);
    this.setSearchPath(searchPath);
  }

  public setClient(client: string): void {
    if (client.length === 0) {
      throw new Error('Client is required to initialize Knex.');
    }

    this.client = client;
  }

  public setDatabase(database: string): void {
    if (database.length === 0) {
      throw new Error('Database is required to initialize Knex.');
    }

    this.database = database;
  }

  public setHost(host: string): void {
    if (host.length === 0) {
      throw new Error('Host is required to initialize Knex.');
    }

    this.host = host;
  }

  public setPassword(password: string): void {
    if (password.length === 0) {
      throw new Error('Password is required to initialize Knex.');
    }

    this.password = password;
  }

  public setPort(port: number): void {
    if (!(port >= 0 && port <= 65535)) {
      throw new Error('A valid port is requird to initialize Knex.');
    }

    this.port = port;
  }

  public setUser(user: string): void {
    if (user.length === 0) {
      throw new Error('User is required to initialize Knex.');
    }

    this.user = user;
  }

  public setSearchPath(searchPath?: string): void {
    this.searchPath = searchPath;
  }

  public getClient(): string {
    return this.client;
  }

  public getDatabase(): string {
    return this.database;
  }

  public getHost(): string {
    return this.host;
  }

  public getPassword(): string {
    return this.password;
  }

  public getPort(): number {
    return this.port;
  }

  public getUser(): string {
    return this.user;
  }

  public getSearchPath(): string | undefined {
    return this.searchPath;
  }

  public getConfigObject(): object {
    return {
      client: this.client,
      connection: {
        database: this.database,
        host: this.host,
        password: this.password,
        port: this.port,
        user: this.user,
      },
      migrations: {
        tableName: 'knex_migrations',
      },
      searchPath: this.searchPath || 'knex, public',
      seeds: {
        directory: './seeds',
      },
    };
  }
}

export default KnexConfig;
