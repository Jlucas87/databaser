import * as Knex from 'knex';
import { Model } from 'objection';
import IDatabaseClient from './clients/IDatabaseClient';

class DatabaseService {
  protected client: IDatabaseClient;

  constructor(client: IDatabaseClient) {
    this.client = client;
  }

  /**
   * Create a new entry in the specified table with the provided data.
   *
   * @param {string} table The targeted table
   * @param {Object} data  The data we wish to insert for the row
   * @return {Model}       The new instance of the inserted item
   */
  public insertItem(model: any, data: object): Promise<Model> {
    return this.client.insertItem(model, data);
  }

  /**
   * Update a row in the specified table with the provided data.
   *
   * @param {string} table The targeted table
   * @param {string} data  The updated row data.
   * @param {number} id    The id of the row we wish to update.
   * @return {Model}       The updated item
   */
  public updateItem(model: any, data: object, id: number): Promise<Model> {
    return this.client.updateItem(model, data, id);
  }

  /**
   * Delete an item in the specified table with the provided id.
   *
   * @param {string} table The targeted table
   * @param {string} data  The id of the row we wish to delete
   * @return {Model}       The deleted item
   */
  public deleteItem(model: any, id: number): Promise<number> {
    return this.client.deleteItem(model, id);
  }

  /**
   * Find a single item in the specified table by id. Optionally return related table
   * data as well.
   *
   * @param {string}  table     The targeted table
   * @param {string}  data      The id of the row we wish to fetch.
   * @param {boolean} relations Fetch the related tables
   * @return {Model}            The model with the matching id.
   */
  public findOne(model: any, id: number, relations?: string): Promise<Model> {
    return this.client.findOne(model, id, relations);
  }

  /**
   * Find all the matching items in the specified table based on the provided search
   * conditions. Optionally return related table data as well.
   *
   * @param {string}  table          The targeted table
   * @param {string}  searchCriteria The search criteria used to find matching rows.
   * @param {boolean} withRelations  Return related table data?
   * @return TBD
   */
  public findAllMatching(model: object, searchCriteria: object, relations?: string, orderBy?: any): Promise<object> {
    return this.client.findAllMatching(model, searchCriteria, relations, orderBy);
  }
}

export default DatabaseService;
