interface IDatabaseClient {
  insertItem(model: object, data: object): Promise<any>;

  updateItem(model: object, data: object, id: number): Promise<any>;

  deleteItem(model: object, id: number): Promise<any>;

  findOne(model: object, id: number, relations?: string): Promise<any>;

  findAllMatching(model: object, searchCriteria: object, relations?: string, orderBy?: any): Promise<any>;

  attach?(modelA: object, modelB: object, idA: number, idB: number, additionalData?: object): Promise<any>;

  detach?(modelA: object, modelB: object, idA: number, idB: number): Promise<any>;
}

export default IDatabaseClient;
