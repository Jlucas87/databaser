import * as Knex from 'knex';
import { Model } from 'objection';
import KnexConfig from '../config/KnexConfig';
import IDatabaseClient from './IDatabaseClient';

class ObjectionClient implements IDatabaseClient {
  protected config?: object;

  constructor(config?: KnexConfig) {
    this.config = config;

    // Initialize Objection to use Knex
    if (config) {
      const db = Knex(config.getConfigObject());
      Model.knex(db);
    }
  }

  /**
   * Create a new entry in the specified table with the provided data.
   *
   * @param {string} table The targeted table
   * @param {Object} data  The data we wish to insert for the row
   * @return {Model}       The new instance of the inserted item
   */
  public insertItem(model: any, data: object): Promise<Model> {
    return model
      .query()
      .insert(data);
  }

  /**
   * Update a row in the specified table with the provided data.
   *
   * @param {string} table The targeted table
   * @param {string} data  The updated row data.
   * @param {number} id    The id of the row we wish to update.
   * @return {Model}       The updated item
   */
  public updateItem(model: any, data: object, id: number): Promise<Model> {
    return model
      .query()
      .patchAndFetchById(id, data);
  }

  /**
   * Delete an item in the specified table with the provided id.
   *
   * @param {string} table The targeted table
   * @param {string} data  The id of the row we wish to delete
   * @return {Model}       The deleted item
   */
  public deleteItem(model: any, id: number): Promise<number> {
    return model
      .query()
      .deleteById(id);
  }

  /**
   * Find a single item in the specified table by id. Optionally return related table
   * data as well.
   *
   * @param {string}  table     The targeted table
   * @param {string}  data      The id of the row we wish to fetch.
   * @param {boolean} relations Fetch the related tables
   * @return {Model}            The model with the matching id.
   */
  public findOne(model: any, id: number, relations?: string): Promise<Model> {
    // Fetch with relations
    let fetchedItem;
    if (relations) {
      fetchedItem = model
        .relatedQuery(relations)
        .findById(id);
    } else {
      fetchedItem = model
        .query()
        .findById(id);
    }

    return fetchedItem;
  }

  /**
   * Find all the matching items in the specified table based on the provided search
   * conditions. Optionally return related table data as well.
   *
   * @param {string}  table          The targeted table
   * @param {string}  searchCriteria The search criteria used to find matching rows.
   * @param {boolean} relations      Fetch the related tables
   * @return {Array<Model>}          The array of models
   */
  public findAllMatching(model: any, searchCriteria: object, relations?: string, orderBy?: any): Promise<[]> {
    let fetchedItem;

    // Fetch with relations
    if (relations) {
      if (orderBy) {
        fetchedItem = model
          .relatedQuery(relations)
          .where(searchCriteria)
          .orderBy(orderBy);
      } else {
        fetchedItem = model
          .relatedQuery(relations)
          .where(searchCriteria);
      }
    } else {
      if (orderBy) {
        fetchedItem = model
          .query()
          .where(searchCriteria)
          .orderBy(orderBy);
      } else {
        fetchedItem = model
          .query()
          .where(searchCriteria);
      }
    }

    return fetchedItem;
  }
}

export default ObjectionClient;
