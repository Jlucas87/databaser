# databaser
Databaser is a library for easily interacting with different types of databases. You can use your existing Objection or Bookshelf models directly with this library. You will be provided with a DatabaseService object containing a number of helpful functions for common operations; such as inserting, updating, fetching, and more.

*Please note, this library is currently in beta and only the objection client is available at present.*

### Configuration

To start using databaser, you'll need to first decide which underlying client you wish to use. The supported clients will include Objection, Bookshelf, and Knex. First, you'll need to pass in connection details for your database to initialize each client. Afterwards, if you're using a Bookshelf or Objection client, just pass in an appropriate model each time you call a function on the DatabaseService. To begin, import the client factory as shown below:

```javascript
const { DatabaseFactory } = require('databaser');
// ...
const databaseFactory = new DatabaseFactory();
```

##### Configure for use with Objection:

To use databaser with objection, just call the appropriate method on the factory to create a new DatabaseService object with an underlying objection client:

```javascript
const databaseService = databaseFactory.generateServiceUsingObjectionClient(
    client, // Type of database client, such as mysql or postgres
    host, // The url of the database
    port, // The port used to connect to the database
    database, // Connect to this specific database on the host
    user, // Connect with this user
    password, // User's password
    searchPath // Optional parameter used for postgres
);
```

##### Configure for use with Bookshelf Client:
*Coming Soon*

##### Configure for user with Knex Client:
*Coming Soon*

### Database Service Operations

##### Inserting new rows:

In order to insert a new row, call the "insertItem" function on the databaseService. The first parameter is the model. The second parameter is an object that contains the data you wish to insert into the table. Here is an example using a hypothetical user model:

```javascript
const User = require('./models/User');
const userData = {
    first_name: 'Jean-Luc',
    last_name: 'Picard',
    dob: 'July 23rd, 2305'
};
const newItem = await databaseService.insertItem(User, userData);
```

##### Updating rows:

In order to update a row, call the "updateItem" function on the databaseService. The first parameter is the model. The second parameter is an object that contains updated items. The third parameter is the id of the row you wish to update. Let's use our hypothetical user again to demonstrate updates:

```javascript
const User = require('./models/User');
const userId = 1;
const userData = {
    first_name: 'William',
    last_name: 'Riker',
    dob: 'August 19th, 2335'
};
const newItem = await databaseService.updateItem(User, userData, userId);
```

##### Fetching items by id:

In order to fetch a specific item from a table, call the "findById" function on the databaseService. The first parameter is the model. The second parameter is the id of the row you wish to fetch. The third parameter (optional) is a string indicating any related tables you wish to include in the result set. Let's fetch the William Riker user from above:

```javascript
const User = require('./models/User');
const userId = 1;
const related = 'ships';
const newItem = await databaseService.findById(User, userId, related);
```

##### Fetching multiple items by criteria:

In order to fetch multiple items from a table, call the "findAllMatching" function on the databaseService. The first parameter is the model. The second parameter is the searchCriteria used to filter the results. The third parameter (optional) is a string indicating any related tables you wish to include in the result set. The fourth parameter (optional) is used to order the result set. Let's fetch all the users from the series "ST:TNG":

```javascript
const User = require('./models/User');
const searchCriteria = {
    series: 'ST:TNG'
}
const related = 'ships';
const orderBy = 'last_name';
const newItem = await databaseService.findAllMatching(User, searchCriteria, related, orderBy);
```

##### Deleting rows:

In order to delete a row, call the "deleteItem" function on the databaseService. The first parameter is the model. The second parameter is the id of the row you wish to delete. Let's try deleting the same user from above:

```javascript
const User = require('./models/User');
const userId = 1;
const newItem = await databaseService.updateItem(User, userId);
```

##### More to come...
